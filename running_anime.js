const canvas = document.getElementById('gameCanvas');
const ctx = canvas.getContext('2d');

const spriteWidth = 50;
const spriteHeight = 50;
const groundLevel = canvas.height - spriteHeight;

let spriteX = 100;
let spriteY = groundLevel;
let spriteVelocityY = 0;
let gravity = 1;
let isJumping = false;

let obstacles = [];
let objects = [];
let score = 0;

function createObstacle() {
    const size = Math.random() * 30 + 20;
    obstacles.push({
        x: canvas.width,
        y: groundLevel,
        width: size,
        height: spriteHeight,
    });
}

function createObject() {
    const size = 20;
    objects.push({
        x: canvas.width,
        y: groundLevel,
        width: size,
        height: size,
    });
}

function update() {
    if (spriteY < groundLevel) {
        spriteVelocityY += gravity;
        isJumping = true;
    } else {
        spriteVelocityY = 0;
        spriteY = groundLevel;
        isJumping = false;
    }
    spriteY += spriteVelocityY;

    obstacles.forEach(obstacle => obstacle.x -= 5);
    objects.forEach(object => object.x -= 5);

    obstacles = obstacles.filter(obstacle => obstacle.x + obstacle.width > 0);
    objects = objects.filter(object => object.x + object.width > 0);

    
    obstacles.forEach(obstacle => {
        if (
            spriteX < obstacle.x + obstacle.width &&
            spriteX + spriteWidth > obstacle.x &&
            spriteY < obstacle.y + obstacle.height &&
            spriteY + spriteHeight > obstacle.y
        ) {
            alert('Game Over! Score: ' + score);
            document.location.reload();
        }
    });
    objects.forEach((object, index) => {
        if (
            spriteX < object.x + object.width &&
            spriteX + spriteWidth > object.x &&
            spriteY < object.y + object.height &&
            spriteY + spriteHeight > object.y
        ) {
            score++;
            objects.splice(index, 1);
        }
    });
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = 'blue';
    ctx.fillRect(spriteX, spriteY, spriteWidth, spriteHeight);

    ctx.fillStyle = 'red';
    obstacles.forEach(obstacle => {
        ctx.fillRect(obstacle.x, obstacle.y, obstacle.width, obstacle.height);
    });

    ctx.fillStyle = 'green';
    objects.forEach(object => {
        ctx.fillRect(object.x, object.y, object.width, object.height);
    });
    ctx.fillStyle = 'black';
    ctx.fillText('Score: ' + score, 10, 20);
}

function gameLoop() {
    update();
    draw();
    requestAnimationFrame(gameLoop);
}

document.addEventListener('keydown', event => {
    if (event.code == 'Space' && !isJumping) {
        spriteVelocityY = -10;
        isJumping = true;
    }
});

setInterval(createObstacle, 2000);
setInterval(createObject, 3000);

gameLoop();

