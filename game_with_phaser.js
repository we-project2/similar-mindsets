let gameScene = new Phaser.Scene('Game');

// Initialize game variables
gameScene.init = function() {
    this.score = 0;
    this.gameOver = false;
    this.enemyTimer = 0;
    this.enemyInterval = 1000;
    this.randomEnemyInterval = Math.random() * 1000 + 500;
    this.jumpVelocity = -330; // Fixed jump velocity
};

// Preload assets
gameScene.preload = function() {
    this.load.image('background', 'assets/background.jpeg');
    this.load.image('player', 'assets/shadow_dog_single.png');
    this.load.image('platform', 'assets/layer-5.png');
    this.load.image('enemy', 'assets/enemy_cropped.png');
    this.load.audio('bgm', 'assets/music.mp3');
};

// Create game objects
gameScene.create = function() {
    // Background
    this.bg = this.add.tileSprite(0, 0, 2400, 632, 'background').setOrigin(0, 0);

    // Player
    this.player = this.physics.add.sprite(70, this.sys.game.config.height - 200, 'player').setScale(0.2, 0.25);
    this.player.setCollideWorldBounds(true);
    this.player.setBounce(0.2);

    // Platform (ground)
    this.platforms = this.physics.add.staticGroup(); // Static group for platforms
    let ground = this.platforms.create(this.sys.game.config.width / 2, this.sys.game.config.height - 50, 'platform').setScale(2, 0.5);
    ground.refreshBody(); // Refresh the body to reflect the new size

    this.physics.add.collider(this.player, this.platforms);

    // Enemies group
    this.enemies = this.physics.add.group({
        immovable: true, // Make enemies immovable
        allowGravity: false // Disable gravity for enemies
    });

    // Input handling
    this.cursors = this.input.keyboard.createCursorKeys();
    
    // Background music
    this.music = this.sound.add('bgm');
    this.music.play({ loop: true });



    this.physics.add.collider(this.enemies, this.platforms);
    this.physics.add.collider(this.player, this.enemies, this.detectCollision, null, this);

    // Display score text
    this.scoreText = this.add.text(20, 20, 'Score: 0', {
        fontSize: '20px',
        fill: '#fff'
    });
};

// Collision detection
gameScene.detectCollision = function(player, enemy) {
    this.gameOver = true;
    this.music.stop();
};

// Update game objects
gameScene.update = function(time, deltaTime) {
    if (this.gameOver) {
        return;
    }

    // Background movement
    this.bg.tilePositionX += 7;

    // Player movement
    if (this.cursors.left.isDown) {
        this.player.setVelocityX(-160);
    } else if (this.cursors.right.isDown) {
        this.player.setVelocityX(160);
    } else {
        this.player.setVelocityX(0);
    }

    if (this.cursors.up.isDown && this.player.body.touching.down) {
        this.player.setVelocityY(this.jumpVelocity);
    }

    // Enemies handling
    this.enemyTimer += deltaTime;
    if (this.enemyTimer > this.enemyInterval + this.randomEnemyInterval) {
        let enemy = this.enemies.create(this.sys.game.config.width, this.sys.game.config.height - 150, 'enemy');
        enemy.setScale(0.3, 0.5);
        enemy.setVelocityX(-200);
        this.randomEnemyInterval = Math.random() * 1000 + 500;
        this.enemyTimer = 0;
    }

    this.enemies.children.iterate(enemy => {
        if (enemy.x < 0 - enemy.width) {
            enemy.destroy();
            this.score++;
            this.scoreText.setText('Score: ' + this.score);
        }
    });

    // Display game over text if game over
    if (this.gameOver) {
        this.add.text(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'GAME OVER\nScore: ' + this.score, {
            fontSize: '40px',
            fill: '#fff',
            align: 'center'
        }).setOrigin(0.5, 0.5);
    }
};

// Game configuration
let config = {
    type: Phaser.AUTO,
    width: 1364,
    height: 632,
    scene: gameScene,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    }
};

// Create the game
let game = new Phaser.Game(config);

